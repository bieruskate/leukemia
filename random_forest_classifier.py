from sklearn.tree import DecisionTreeClassifier

from votingclassifier import VotingClassifier


class CustomRandomForest(VotingClassifier):
    def __init__(self,
                 n_estimators=20,
                 max_depth=6,
                 k_out=100,
                 use_k_split=False):
        self.n_estimators = n_estimators
        self.max_depth = max_depth

        forest = [DecisionTreeClassifier(max_depth=max_depth) for _ in range(n_estimators)]

        super().__init__(classifiers=forest, use_k_split=use_k_split, k_out=k_out,
                         randomize_training_sets=not use_k_split)
