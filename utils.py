import itertools

import numpy as np
import matplotlib.pyplot as plt


def get_all_combinations(base_list, starting_size=1):
    combinations = []
    for i in range(starting_size, len(base_list) + 1):
        for j in list(itertools.combinations(base_list, i)):
            combinations.append(j)
    return combinations


def get_class_names(classes):
    return [c.__class__.__name__ for c in classes]


def plot_grid_search(cv_results, grid_param_1, grid_param_2, name_param_1, name_param_2):
    # Get Test Scores Mean and std for each grid search
    scores_mean = cv_results['mean_test_score']
    scores_mean = np.array(scores_mean).reshape(len(grid_param_2), len(grid_param_1))

    # Plot Grid search scores
    _, ax = plt.subplots(1, 1)

    # Param1 is the X-axis, Param 2 is represented as a different curve (color line)
    for idx, val in enumerate(grid_param_2):
        ax.plot(grid_param_1, scores_mean[idx, :], '-o', label=name_param_2 + ': ' + str(val))

    ax.set_xlabel(name_param_1)
    ax.set_ylabel('Poprawność')
    ax.legend(loc="best")
    ax.grid('on')


def plot_grid_search_heatmap(cv_results, grid_param_1, grid_param_2, name_param_1, name_param_2):
    scores_mean = cv_results['mean_test_score']
    scores_mean = np.array(scores_mean).reshape(len(grid_param_2), len(grid_param_1))

    plt.subplots_adjust(left=.2, right=0.95, bottom=0.15, top=0.95)
    plt.imshow(scores_mean, interpolation='nearest', cmap=plt.cm.hot)
    plt.xlabel(name_param_1)
    plt.ylabel(name_param_2)
    plt.colorbar()
    plt.xticks(np.arange(len(grid_param_1)), grid_param_1)
    plt.yticks(np.arange(len(grid_param_2)), grid_param_2)
    plt.show()
