from classifier_base import ClassifierBase
from mixins import MultiClassMixin


class DynamicMultiClassifier(ClassifierBase, MultiClassMixin):
    def __init__(self,
                 classifiers,
                 neighbours=100):
        self.neighbours = neighbours
        self.classifiers = classifiers

    def fit(self, X, y):
        super().fit(X, y)

        for classifier in self.classifiers:
            classifier.fit(X, y)

    def predict(self, X):
        if hasattr(X, 'values'):
            X = X.values

        return self._predict_with_best_classifier_for_each_object(X)

    def _predict_with_best_classifier_for_each_object(self, X):
        weights = self.set_weights_with_nearest_neighbours(X, self.X_, self.y_, self.classifiers, self.neighbours)

        results = []
        for i, object_weights in enumerate(weights):
            best_clf_index = object_weights.index(max(object_weights))
            label = self.classifiers[best_clf_index].predict([X[i, :]])[0]
            results.append(label)

        return results
