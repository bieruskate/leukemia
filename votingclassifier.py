from multiprocessing.pool import Pool

from numpy import random
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

from classifier_base import ClassifierBase
from mixins import MultiClassMixin


class VotingClassifier(ClassifierBase, MultiClassMixin):
    def __init__(self,
                 classifiers=None,
                 weighted_voting=True,
                 use_neighbours=False,
                 neighbours=100,
                 select_best=False,
                 best_percentage=50,
                 logging_on=False,
                 randomize_training_sets=False,
                 k_out=100,
                 use_workers=False,
                 use_k_split=False):
        self.k_out = k_out
        self.neighbours = neighbours
        self.use_k_split = use_k_split
        self.use_workers = use_workers
        self.randomize_training_sets = randomize_training_sets
        self.weighted_voting = weighted_voting
        self.use_neighbours = use_neighbours

        if classifiers:
            self.classifiers = classifiers
        else:
            self.classifiers = [SVC(), DecisionTreeClassifier(), KNeighborsClassifier()]

        self.select_best = select_best

        if select_best:
            self.best_percentage = best_percentage

        self.logging_on = logging_on
        self.weights = None
        self.training_subsets = []

    def fit(self, X, y):
        super().fit(X, y)

        if self.select_best:
            self.classifiers = self.choose_best_classifiers(self.classifiers, X, y, percent=self.best_percentage)

        self._fit_classifiers()

    def predict(self, X):
        if not self.randomize_training_sets and not self.use_k_split:
            if self.weighted_voting and self.use_neighbours:
                self.weights = self.set_weights_with_nearest_neighbours(X, self.X_, self.y_, self.classifiers,
                                                                        self.neighbours)
            elif self.weighted_voting and not self.use_neighbours:
                self.weights = self.prepare_weights(self.X_, self.y_, self.classifiers, cv=10)

        if self.logging_on:
            print(self.weights)

        predictions = self._predict_by_all_classifiers(X)
        return self._combine_classifiers(predictions)

    def _combine_classifiers(self, predictions):
        supports = self.get_supports(predictions, self.weights)
        return [self.get_best_class_for_support_dict(support) for support in supports]

    def _predict_by_all_classifiers(self, X):
        return [clf.predict(X) for clf in self.classifiers]

    def _fit_classifiers(self):
        subsets = self.get_shuffled_indices_splits_in_range(self.X_.shape[0],
                                                            len(self.classifiers)) if self.use_k_split else []

        if self.use_k_split or self.randomize_training_sets:
            self.weights = []

        if self.use_workers:
            with Pool() as pool:
                indices = [i for i in range(len(self.classifiers))]
                self.classifiers = pool.map(self._fit_classifier, zip(indices, subsets))
        else:
            for i, clf in enumerate(self.classifiers):
                if self.use_k_split:
                    self._fit_classifier(i, subsets[i])
                else:
                    self._fit_classifier(i, [])

    def _fit_classifier(self, clf_index, subset):
        if self.use_k_split:

            current_subset = [i for i in range(self.X_.shape[0]) if i not in subset]

            new_X = self.X_[current_subset]
            new_y = self.y_[current_subset]

            self.classifiers[clf_index].fit(new_X, new_y)
            self.weights.append(self.get_weight(new_X, new_y, self.classifiers[clf_index], 3))

        elif self.randomize_training_sets:
            random_subset = random.choice(self.X_.shape[0], self.X_.shape[0] - self.k_out, replace=False)
            new_X = self.X_[random_subset]
            new_y = self.y_[random_subset]

            self.classifiers[clf_index].fit(new_X, new_y)
            self.weights.append(self.get_weight(new_X, new_y, self.classifiers[clf_index], 5))
        else:
            self.classifiers[clf_index].fit(self.X_, self.y_)

        return self.classifiers[clf_index]
