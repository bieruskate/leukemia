from classifier_base import ClassifierBase
from mixins import ElemMultiClassMixin


class ElementaryMultiClassifier(ClassifierBase, ElemMultiClassMixin):
    def __init__(self,
                 classifiers=None,
                 rule='median',
                 class_offset=0):
        self.class_offset = class_offset
        self.rule = rule
        self.classifiers = classifiers

    def fit(self, X, y):
        super().fit(X, y)
        self._fit_all_classifiers()

    def predict(self, X):
        probabilities = [clf.predict_proba(X) for clf in self.classifiers]

        results = [self.process_probabilities(probabilities_set, self.rule, self.class_offset)
                   for probabilities_set in zip(*probabilities)]
        return results

    def _fit_all_classifiers(self):
        for clf in self.classifiers:
            clf.fit(self.X_, self.y_)
