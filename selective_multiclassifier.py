import numpy as np
from sklearn.cluster import KMeans

from classifier_base import ClassifierBase
from mixins import MultiClassMixin


class SelectiveMultiClassifier(ClassifierBase, MultiClassMixin):
    def __init__(self,
                 classifiers,
                 n_clusters=4,
                 cv=5):
        self.cv = cv
        self.n_clusters = n_clusters
        self.classifiers = classifiers
        self.classifiers_subspaces = {}
        self.kmeans = None

    def fit(self, X, y):
        super().fit(X, y)

        clusters_indices = self._get_clusters_with_indices_of_objects_in_each(X)
        self._set_up_best_classifiers_for_each_subspace(clusters_indices, X, y)

        for classifier in self.classifiers_subspaces.values():
            classifier.fit(X, y)

    def _get_clusters_with_indices_of_objects_in_each(self, X):
        self.kmeans = KMeans(n_clusters=self.n_clusters, random_state=0).fit(X)
        clusters_indices = {i: np.where(self.kmeans.labels_ == i)[0] for i in range(self.kmeans.n_clusters)}
        return clusters_indices

    def predict(self, X):
        if hasattr(X, 'values'):
            X = X.values

        labels = self.kmeans.predict(X)
        return [self.classifiers_subspaces[label].predict([X[i, :]])[0]
                for i, label in enumerate(labels)]

    def _set_up_best_classifiers_for_each_subspace(self, clusters_indices, X, y):
        for cluster_number, indices in clusters_indices.items():
            best_in_subspace = self.choose_best_classifier(self.classifiers, X[indices], y[indices],
                                                           self.cv)
            self.classifiers_subspaces[cluster_number] = best_in_subspace
