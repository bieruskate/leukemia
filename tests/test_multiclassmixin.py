from unittest import TestCase

from sklearn import datasets
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

from mixins import MultiClassMixin


class MultiClassMixinTest(TestCase, MultiClassMixin):
    def test_should_compute_weights_of_classifiers(self):
        self._set_up_data_for_classification()
        expected = [0.75367647058823539, 0.79983660130718948]

        weights = self.prepare_weights(self.X, self.y, self.classifiers, cv=3)
        self.assertListEqual(expected, weights)

    def test_should_return_class_with_highest_weighted_vote(self):
        support = {
            'A': 0.35,
            'B': 0.2,
            'C': 0.9,
            'D': 0.11
        }
        best_class = self.get_best_class_for_support_dict(support)
        self.assertEqual('C', best_class)

    def test_should_return_correct_supports(self):
        predictions = [
            ['A', 'B'],
            ['A', 'C'],
            ['D', 'C'],
        ]
        weights = [1, 0.5, 1.25]

        expected = [
            {'A': 1.5, 'D': 1.25},
            {'B': 1, 'C': 1.75}
        ]

        supports = self.get_supports(predictions, weights)
        self.assertEqual(expected, supports)

    def test_should_prepare_weighted_predictions(self):
        weights = [0.25, 0.5, 1, 0.75]
        prediction_set = ('A', 'B', 'A', 'C')
        expected = {
            'A': 1.25,
            'B': 0.5,
            'C': 0.75
        }

        weighted_classes = self.get_weighted_predictions(prediction_set, weights, None)
        self.assertEqual(expected, weighted_classes)

    def test_should_weight_all_classes_equal_if_weights_array_is_none(self):
        prediction_set = ('A', 'B', 'A', 'C', 'A', 'B')
        expected = {
            'A': 3,
            'B': 2,
            'C': 1
        }

        weighted_classes = self.get_weighted_predictions(prediction_set, None, 0)
        self.assertEqual(expected, weighted_classes)

    def test_should_set_weights_based_on_nearest_neighbour(self):
        self._set_up_data_for_classification()

        for clf in self.classifiers:
            clf.fit(self.X, self.y)

        weights = self.set_weights_with_nearest_neighbours(self.X[:2], self.X, self.y, self.classifiers, nn=100)
        expected = [[0.85858585858585856, 0.87878787878787878],
                    [0.85858585858585856, 0.87878787878787878]]

        self.assertEqual(expected, weights)

    def test_should_return_correct_percent_of_classifiers(self):
        classifiers = [
            SVC(),
            KNeighborsClassifier(),
            GaussianNB(),
            DecisionTreeClassifier(),
            LogisticRegression(),
        ]
        self._set_up_data_for_classification()

        chosen_classifiers = self.choose_best_classifiers(classifiers, self.X, self.y, cv=10, percent=40)
        expected_best_classifiers = [classifiers[0], classifiers[2]]

        self.assertEqual(expected_best_classifiers, chosen_classifiers)

    def test_should_return_best_classifier(self):
        classifiers = [
            SVC(),
            KNeighborsClassifier(),
            GaussianNB(),
            DecisionTreeClassifier(),
            LogisticRegression(),
        ]
        self._set_up_data_for_classification()
        best = self.choose_best_classifier(classifiers, self.X, self.y, cv=10)

        self.assertEqual(classifiers[0], best)

    def test_should_return_splits_of_shuffled_indices_in_given_range(self):
        range_of_indices = 20
        splits = self.get_shuffled_indices_splits_in_range(range_of_indices, 5)
        numbers = []
        for split in splits:
            numbers.extend(split)

        numbers.sort()

        self.assertEqual([i for i in range(range_of_indices)], numbers)

    def _set_up_data_for_classification(self):
        self.classifiers = [
            KNeighborsClassifier(),
            SVC()
        ]
        iris = datasets.load_iris()
        self.X = iris.data[:, :2]
        self.y = iris.target
