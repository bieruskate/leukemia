from unittest import TestCase

from utils import get_all_combinations


class TestUtils(TestCase):
    def test_should_return_all_combinations_for_list(self):
        expected = [(1,), (2,), (3,), (1, 2), (1, 3), (2, 3), (1, 2, 3)]
        combinations = get_all_combinations([1, 2, 3])
        assert expected == combinations

    def test_should_return_all_combinations_starting_from_set_size(self):
        expected = [(1, 2), (1, 3), (2, 3), (1, 2, 3)]
        combinations = get_all_combinations([1, 2, 3], 2)
        assert expected == combinations
