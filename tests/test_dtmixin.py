from unittest import TestCase

import numpy as np

from mixins import DTMixin


class ElemMultiClassMixinTest(TestCase, DTMixin):
    def test_should_return_label_for_most_similar_template(self):
        templates = {
            'a': np.array([
                [1, 2, 3],
                [4, 5, 6],
                [7, 8, 9]
            ]),
            'b': np.array([
                [9, 9, 9],
                [7, 5, 6],
                [0, 0, 0]
            ]),
            'c': np.array([
                [1, 2, 3],
                [4, 5, 6],
                [7, 4, 4]
            ]),
        }

        profile = np.array([
            [1, 2, 3],
            [4, 4, 6],
            [7, 8, 9]
        ])

        label = self.get_class_by_comparing_to_templates(templates, profile)
        assert label == 'a'
