from unittest import TestCase

from mixins import ElemMultiClassMixin


class ElemMultiClassMixinTest(TestCase, ElemMultiClassMixin):
    def setUp(self):
        self.probabilities_set = [[1, 2, 3],
                                  [2, 4, 6],
                                  [2, 1, 5]]

    def test_should_use_add_rule_for_probabilities(self):
        computed = self.compute_rule_on_probabilities(self.probabilities_set, 'sum')

        expected = [5, 7, 14]
        self.assertEqual(expected, computed)

    def test_should_use_min_rule_for_probabilities(self):
        computed = self.compute_rule_on_probabilities(self.probabilities_set, 'min')

        expected = [1, 1, 3]
        self.assertEqual(expected, computed)

    def test_should_use_max_rule_for_probabilities(self):
        computed = self.compute_rule_on_probabilities(self.probabilities_set, 'max')

        expected = [2, 4, 6]
        self.assertEqual(expected, computed)

    def test_should_use_average_rule_for_probabilities(self):

        computed = self.compute_rule_on_probabilities(self.probabilities_set, 'avg')

        expected = [5/3, 7/3, 14/3]
        self.assertEqual(expected, computed)

    def test_should_use_median_rule_for_probabilities(self):

        computed = self.compute_rule_on_probabilities(self.probabilities_set, 'median')

        expected = [2, 2, 5]
        self.assertEqual(expected, computed)
