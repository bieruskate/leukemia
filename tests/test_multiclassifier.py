from unittest import TestCase

from mock import patch
from sklearn import datasets
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

from votingclassifier import VotingClassifier


class VotingClassifierTest(TestCase):
    def setUp(self):
        self._set_up_data_for_classification()
        self.model = VotingClassifier(classifiers=self.classifiers)

    @patch.object(VotingClassifier, 'set_weights_with_nearest_neighbours', return_value=[0.5, 0.7])
    def test_should_call_set_weights_with_nn_if_use_nn_is_set(self, mock):
        self.model.use_neighbours = True
        self._fit_and_predict()

        self.assertTrue(mock.called)

    @patch.object(VotingClassifier, 'set_weights_with_nearest_neighbours')
    def test_should_not_call_set_weights_with_nn_if_use_nn_is_not_set(self, mock):
        self.model.use_neighbours = False
        self._fit_and_predict()

        self.assertFalse(mock.called)

    @patch.object(VotingClassifier, 'prepare_weights', return_value=[0.5, 0.7])
    def test_should_use_weighted_voting_by_default(self, mock):
        self.model = VotingClassifier(classifiers=self.classifiers)
        self._fit_and_predict()

        self.assertTrue(mock.called)

    @patch.object(VotingClassifier, 'prepare_weights', return_value=[0.5, 0.7])
    def test_should_not_use_weighted_voting_when_disabled(self, mock):
        self.model = VotingClassifier(classifiers=self.classifiers)
        self.model.weighted_voting = False
        self._fit_and_predict()

        self.assertFalse(mock.called)

    def _fit_and_predict(self):
        self.model.fit(self.X, self.y)
        self.model.predict([self.X[0]])

    def _set_up_data_for_classification(self):
        self.classifiers = [
            KNeighborsClassifier(),
            SVC()
        ]
        iris = datasets.load_iris()
        self.X = iris.data[:, :2]
        self.y = iris.target
