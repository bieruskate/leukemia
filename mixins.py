from operator import itemgetter
from sklearn import metrics

import numpy as np
from scipy.spatial import distance
from sklearn.model_selection import cross_val_score
from sklearn.neighbors import NearestNeighbors


class MultiClassMixin:
    @staticmethod
    def get_weight(X, y, classifier, cv):
        return cross_val_score(estimator=classifier, X=X, y=y, cv=cv).mean()

    @staticmethod
    def prepare_weights(X, y, classifiers, cv):
        return [cross_val_score(estimator=clf, X=X, y=y, cv=cv).mean()
                for clf in classifiers]

    @staticmethod
    def choose_best_classifiers(classifiers, X, y, cv=10, percent=50):
        number_of_clfs = int(round(len(classifiers) * (percent / 100), 0))

        weights = MultiClassMixin.prepare_weights(X, y, classifiers, cv)
        clfs_weights = [{'classifier': classifiers[i], 'weight': weight} for i, weight in enumerate(weights)]

        clfs_sorted_by_performance = [clf_weight['classifier'] for clf_weight in
                                      reversed(sorted(clfs_weights, key=itemgetter('weight')))]
        return clfs_sorted_by_performance[:number_of_clfs]

    @staticmethod
    def choose_best_classifier(classifiers, X, y, cv=10):
        return MultiClassMixin.choose_best_classifiers(classifiers, X, y, cv, int(1 / len(classifiers) * 100))[0]

    @staticmethod
    def get_best_class_for_support_dict(support_dict):
        best_class = None
        best_support = 0

        for key, value in support_dict.items():
            if value > best_support:
                best_support = value
                best_class = key

        return best_class

    @staticmethod
    def get_supports(predictions, weights):
        return [MultiClassMixin.get_weighted_predictions(result_set, weights, index)
                for index, result_set in enumerate(zip(*predictions))]

    @staticmethod
    def get_weighted_predictions(result_set, weights, index):
        weights = np.asarray(weights)

        weighted_classes = {}
        for i, predicted_class in enumerate(result_set):
            current_weight = weighted_classes.get(predicted_class, 0)
            array_type = len(weights.shape)

            if array_type == 2:
                weighted_classes[predicted_class] = current_weight + weights[index][i]
            elif array_type == 1:
                weighted_classes[predicted_class] = current_weight + weights[i]
            elif array_type == 0:
                weighted_classes[predicted_class] = current_weight + 1
        return weighted_classes

    @staticmethod
    def set_weights_with_nearest_neighbours(X, X_, y_, classifiers, nn):
        neighbours = NearestNeighbors(n_neighbors=nn, algorithm='ball_tree').fit(X_)
        indices = neighbours.kneighbors(X, return_distance=False)

        indices = indices[:, 1:]
        x_neighbours = X_[indices]
        y_neighbours = y_[indices]

        return [MultiClassMixin.prepare_weights_with_accuracy_score(x, y, classifiers)
                for x, y in zip(x_neighbours, y_neighbours)]

    @staticmethod
    def prepare_weights_with_accuracy_score(X, y, classifiers):
        return [metrics.accuracy_score(clf.predict(X), y)
                for clf in classifiers]

    @staticmethod
    def get_shuffled_indices_splits_in_range(to_index, splits):
        indices = np.arange(to_index)
        np.random.shuffle(indices)
        return np.array_split(indices, splits)


class ElemMultiClassMixin:
    def process_probabilities(self, probabilities_set, rule, class_offset):
        computed_list = self.compute_rule_on_probabilities(probabilities_set, rule)

        index, value = max(enumerate(computed_list), key=itemgetter(1))

        predicted_class = index + class_offset
        return predicted_class

    @staticmethod
    def compute_rule_on_probabilities(probabilities_set, rule):
        compute_fun = {
            'min': np.min,
            'max': np.max,
            'product': np.product,
            'sum': np.sum,
            'avg': np.mean,
            'median': np.median
        }
        return [compute_fun[rule](class_probabilities)
                for class_probabilities in zip(*probabilities_set)]


class DTMixin:
    @staticmethod
    def get_class_by_comparing_to_templates(templates, profile):
        results = {label: np.linalg.norm(np.array(template) - np.array(profile))
                   for label, template in templates.items()}

        return min(results, key=results.get)
