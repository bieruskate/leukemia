from pprint import pprint

import numpy
import pandas as pandas
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import cross_val_score
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

from elementary_multiclassifier import ElementaryMultiClassifier


def main():
    numpy.random.seed(1234)
    data = pandas.read_csv("data/bialaczka.csv", header=0)

    data.drop(['Unnamed: 0', 'Unnamed: 1'], axis=1, inplace=True)

    prediction_var = [str(i) for i in range(1, 21) if i not in [13]]

    X = data[prediction_var]
    y = data.Class
    y = y.apply(lambda x: x - 1)

    classifiers = [
        SVC(probability=True, kernel='poly', C=.5),
        KNeighborsClassifier(n_neighbors=13),
        LogisticRegression(),
        GaussianNB(),
        DecisionTreeClassifier(max_depth=6),
    ]

    model = ElementaryMultiClassifier(classifiers)
    scores = cross_val_score(estimator=model, X=X, y=y, cv=10, n_jobs=-1)

    pprint(scores)
    print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))


if __name__ == "__main__":
    main()
