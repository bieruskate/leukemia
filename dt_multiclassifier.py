import numpy as np

from classifier_base import ClassifierBase
from mixins import DTMixin


class DTMultiClassifier(ClassifierBase, DTMixin):
    def __init__(self,
                 classifiers=None):
        self.classifiers = classifiers
        self.decision_templates = {}

    def fit(self, X, y):
        super().fit(X, y)
        self._fit_all_classifiers()

        groups = {}
        for label in y.unique():
            groups[label] = []

        for i in range(X.shape[0]):
            groups[y.values[i]].append(X.values[i])

        for label, objects in groups.items():
            self.decision_templates[label] = [clf.predict_proba(objects).mean(axis=0) for clf in self.classifiers]

    def predict(self, X):
        results = []
        for prediction_index in range(X.shape[0]):
            decision_profile = [clf.predict_proba([X.values[prediction_index]]).mean(axis=0) for clf in
                                self.classifiers]
            results.append(self.get_class_by_comparing_to_templates(self.decision_templates, decision_profile))

        return results

    def _fit_all_classifiers(self):
        for clf in self.classifiers:
            clf.fit(self.X_, self.y_)
